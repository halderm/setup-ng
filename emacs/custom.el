;; -------------------- admin stuff
;; check os we are running on
(defvar gnulinux-p (string-match "gnu/linux" (symbol-name system-type)))
(defvar macosx-p (string-match "darwin" (symbol-name system-type)))
(defvar mswindows-p (string-match "windows" (symbol-name system-type)))

;; enable server mode for shell client
(server-start)

;; disable menu
(menu-bar-mode -1)

;; set font for linux
(if gnulinux-p
  (set-frame-font "Ubuntu Mono 11" nil t))

;; early install packages
(prelude-require-packages
  (quote
    (org-mime evil-org evil-leader deft magit htmlize evil-surround evil-numbers)))

(setq org-html-use-infojs nil)
(setq org-publish-project-alist
  '(("orgfiles"
    :base-directory "~/orgmode"
    :base-extension "org"
    :publishing-directory "~/public_html"
    :recursive t
    :publishing-function org-html-publish-to-html
    :headline-levels 4
    :auto-preamble t
    :section-numbers t
    :auto-sitemap t
    :sitemap-filename "index.org"
    :sitemap-title "index"
    :html-head-include-default-style nil
    :html-head-include-scripts nil
    :html-head "<link rel=\"stylesheet\" type=\"text/css\" href=\"https://www.pirilampo.org/styles/readtheorg/css/htmlize.css\"/>
         <link rel=\"stylesheet\" type=\"text/css\" href=\"https://www.pirilampo.org/styles/readtheorg/css/readtheorg.css\"/>
         <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>
         <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js\"></script>
         <script type=\"text/javascript\" src=\"https://www.pirilampo.org/styles/lib/js/jquery.stickytableheaders.min.js\"></script>
         <script type=\"text/javascript\" src=\"https://www.pirilampo.org/styles/readtheorg/js/readtheorg.js\"></script>"
  )
  ("orgstatic"
    :base-directory "~/orgmode"
    :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
    :publishing-directory "~/public_html/"
    :recursive t
    :publishing-function org-publish-attachment
  )
  ("org" :components ("orgfiles" "orgstatic"))))

;; function to display ansi colors in buffer
(require 'ansi-color)
(defun display-ansi-colors ()
  (interactive)
  (ansi-color-apply-on-region (point-min) (point-max)))

;; add dired-async-mode
(dired-async-mode 1)

;; -------------------- mac stuff
;; use cmd as meta on mac
(if macosx-p
  (progn
    (setq mac-option-key-is-meta t)
    (setq mac-command-key-is-meta nil)))

;; path and exec-path variable with macports
(if macosx-p
  (progn
    (setenv "PATH" (concat "/opt/local/sbin:/opt/local/bin:" (getenv "PATH")))
    (setq exec-path (append '("/opt/local/sbin") '("/opt/local/bin") exec-path))))

;; -------------------- calendar
;; use european calendar
(add-hook 'calendar-load-hook
  (lambda ()
    (calendar-set-date-style 'european)))

; swiss national holidays
(setq holiday-other-holidays
  '((holiday-fixed 1 1 "Neujahr")
    (holiday-fixed 1 2 "Berchtoldstag")
    (holiday-easter-etc -2 "Karfreitag")
    (holiday-easter-etc 1 "Ostermontag")
    (holiday-easter-etc 39 "Auffahrt")
    (holiday-easter-etc 50 "Pfingstmontag")
    (holiday-fixed 5 1   "Tag der Arbeit")
    (holiday-fixed 8 1  "Nationalfeiertag")
    (holiday-fixed 8 15  "Maria Himmelfahrt")
    (holiday-fixed 11 1  "Allerheiligen")
    (holiday-fixed 12 8  "Allerheiligen")
    (holiday-fixed 12 25 "Weihnachten")
    (holiday-fixed 12 26 "Stephanstag")))

(setq holiday-general-holidays nil)
(setq holiday-local-holidays nil)
(setq holiday-christian-holidays nil)
(setq holiday-hebrew-holidays nil)
(setq holiday-islamic-holidays nil)
(setq holiday-bahai-holidays nil)
(setq holiday-oriental-holidays nil)
(setq holiday-solar-holidays nil)

;; -------------------- plugins
;; htmlize
(require 'htmlize)

;; undo tree
(require 'undo-tree)
(global-undo-tree-mode)

;; deft
(require 'deft)
(setq deft-extension "org")
(setq deft-directory "~/orgmode")
(setq deft-text-mode 'org-mode)
(setq deft-use-filename-as-title t)
(global-set-key (kbd "C-c v") 'deft)

;; babel
(setq org-confirm-babel-evaluate nil)

;; active babel languages
(org-babel-do-load-languages
  'org-babel-load-languages
  '((R . t)
    (emacs-lisp . t)
    (shell . t)
    (python . t)))

;; evil
(require 'evil-leader)
(global-evil-leader-mode)
(evil-leader/set-leader ",")
(evil-leader/set-key
  "b"  'switch-to-buffer
  "e"  'find-file
  "j;" 'kill-emacs
  "jk" 'save-buffer
  "jl" 'save-buffers-kill-terminal
  "k"  'kill-buffer
  "qk" 'avy-goto-char
  "ql" 'avy-goto-line
  "qq" 'avy-goto-word-1
  "u"  'undo-tree-visualize
  "vv" 'evil-normal-state
  "x"  'execute-extended-command)
(require 'evil)
(evil-mode 1)
(require 'evil-org)
(add-hook 'org-mode-hook 'evil-org-mode)
(evil-org-set-key-theme '(navigation insert textobjects additional calendar))
(require 'evil-org-agenda)
(evil-org-agenda-set-keys)
(key-chord-mode -1)

;; disable flyspell mode by default
(setq prelude-flyspell nil)
(setq whitespace-line-column 200)

;; -------------------- org general setup
;; open .org and .org_archive files with org-mode
(add-to-list 'auto-mode-alist '("\\.\\(org\\|org_archive\\)$" . org-mode))

;; useful key mappings
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)

;; shortcut to custom files
(set-register ?e (cons 'file "~/setup-ng/emacs/custom.el"))
(set-register ?o (cons 'file "~/orgmode/life.org"))
(set-register ?w (cons 'file "~/orgmode/work.org"))
(set-register ?k (cons 'file "~/orgmode/knowhow.org"))

;; define default drawers
(setq org-drawers (quote ("properties" "clocktable" "logbook" "clock")))

;; hide leading stars
(setq org-hide-leading-stars 'hidestars)

;; use helm everywhere
(setq org-completion-use-ido nil)
(setq org-outline-path-complete-in-steps nil)

;; use only 1 separator line
(setq org-cycle-separator-lines 1)

;; enforce ordered tasks
(setq org-enforce-todo-checkbox-dependencies t)
(setq org-enforce-todo-dependencies t)
(setq org-track-ordered-property-with-tag "ordered")

;; -------------------- org agenda
;; set agenda file
(setq org-agenda-files (quote ("~/orgmode")))

;; highlight current time in agenda
(add-hook 'org-agenda-mode-hook '(lambda () (hl-line-mode 1 )))

;; display priorities
(setq org-agenda-fontify-priorities
  (quote ((65 (:foreground "firebrick")) (66 (:foreground "blue")) (67 (:foreground "dark green")))))

;; mark weekend
(setq org-agenda-date-weekend (quote (:foreground "yellow" :weight bold)))

;; hide tasks already done
(setq org-agenda-skip-deadline-if-done t)
(setq org-agenda-skip-scheduled-if-done t)

;; hide items tagged with someday
(setq org-agenda-filter-preset '("-someday"))

;; custom agenda views
(setq org-agenda-custom-commands
  (quote
    (("z" "admin tasks" todo "admin" nil)
     ("w" "waiting tasks" todo "waiting" nil)
     ("s" "someday tasks" tags "someday"
       ((org-agenda-filter-preset
         (quote
           ("+someday")))
       (org-agenda-todo-ignore-with-date nil)))
     ("u" "urgent tasks and phone calls"
       ((tags "urgent"
         ((org-agenda-overriding-header "urgent tasks")))
       (tags "phone"
         ((org-agenda-overriding-header "phone calls"))))
     nil nil))))

;; -------------------- org todos
;; set keywords
(setq org-todo-keywords
  (quote ((sequence "todo(t)" "next(n)" "started(s)" "|" "done(d)")
          (sequence "waiting(w@/!)" "hold(h@/!)" "delegated(g@/!)" "|" "canceled(c@/!)")
          (sequence "appt(a)" "|" "proj(p)" "meeting(m)" "admin(z)"))))

;; set tags
(setq org-tag-persistent-alist
  (quote (("home" . ?h) ("work" . ?w) ("computer" . ?c) ("phone" . ?p)
          ("errands" . ?e) ("urgent" . ?u) ("someday" . ?s))))

;; change colors
(setq org-todo-keyword-faces
  (quote (("todo" :foreground "red" :weight bold)
          ("next" :foreground "firebrick" :weight bold)
          ("started" :foreground "dark red" :weight bold)
          ("done" :foreground "dark green" :weight bold)
          ("waiting" :foreground "dark orange" :weight bold)
          ("hold" :foreground "maroon" :weight bold)
          ("delegated" :foreground "forest green" :weight bold)
          ("canceled" :foreground "grey" :weight bold)
          ("appt" :foreground "sienna" :weight bold)
          ("proj" :foreground "blue" :weight bold)
          ("meeting" :foreground "dark goldenrod" :weight bold)
          ("admin" :foreground "royal blue" :weight bold))))

(setq org-priority-faces
  (quote ((?A :foreground "firebrick" :weight bold)
          (?B :foreground "blue")
          (?C :foreground "dark green"))))

;; change font size
(set-face-attribute 'default nil :height 110)

;; fast todos selection
(setq org-use-fast-todo-selection t)

;; add timestamp when done and rescheduled
(setq org-log-done (quote time))
(setq org-log-reschedule (quote time))

;; use logbook drawer for logs
(setq org-log-into-drawer t)

;; -------------------- org refile
;; refile targets
(setq org-refile-targets (quote ((org-agenda-files :maxlevel . 2) (nil :maxlevel . 5))))

;; targets start with the buffer name
(setq org-refile-use-outline-path (quote buffer-name))

(setq org-refile-allow-creating-parent-nodes t)

;; -------------------- org timer
;; save clock in drawer
(setq org-clock-into-drawer "CLOCK")

;; resume clocking tasks when emacs is restarted
(org-clock-persistence-insinuate)

;; sesume clocking task on clock-in if the clock is open
(setq org-clock-in-resume t)

;; don't clock out when moving task to a done state
(setq org-clock-out-when-done nil)

;; persist clock
(setq org-clock-persist t)

;; disable auto clock resolution
(setq org-clock-auto-clock-resolution nil)

;; persist clock
(setq org-publish-use-timestamps-flag nil)

;; -------------------- org capture
;; setup org-capture
(setq org-default-notes-file (concat org-directory "~/orgmode/life.org"))
(define-key global-map "\C-cc" 'org-capture)
(setq org-capture-templates
  '(("t" "task" entry (file+headline "~/orgmode/life.org" "inbox")
     "* todo %?")
    ("c" "task with context" entry (file+headline "~/orgmode/life.org" "inbox")
     "* todo %?\n  %i\n  %a")
    ("i" "immediate admin task" entry (file+headline "~/orgmode/life.org" "inbox")
     "* admin %?" :clock-in t :clock-keep t)
    ("z" "admin task" entry (file+headline "~/orgmode/life.org" "inbox")
     "* admin %?\n  %i\n  %a" :clock-in t :clock-resume t)
    ("j" "journal" entry (file+datetree "~/orgmode/life.org")
     "* %?\nentered on %U\n  %i\n  %a")))

;; -------------------- org column view
;; setup default format
(setq org-columns-default-format "%7todo %50item %10effort{:} %10clocksum")

;; setup default estimates
(setq org-global-properties (quote (("effort_all" . "0:30 1:00 2:00 4:00 8:00"))))

;; setup default browser
(if macosx-p
  (progn
    (setq browse-url-browser-function 'browse-url-default-macosx-browser))
  (progn
    (setq browse-url-browser-function 'browse-url-chrome)))

;; -------------------- gcal
;; show gcalcli agenda in buffer
(defun open-gcal-agenda ()
  "Open my google calendar agenda file. The agenda is displayed in the buffer *gcal*."
  (interactive)
  ;; set name of calendar buffer and location of file containing my agenda
  (shell-command "gcalcli agenda > /tmp/gcal")
  (let ((tmp-buff-name "*gcal*") (cal-file (expand-file-name "/tmp/gcal")))
    ;; switch to calendar buffer
    (switch-to-buffer tmp-buff-name)
    ;; turn off read only to overwrite if buffer exists
    (read-only-mode -1)
    ;; clear buffer
    (erase-buffer)
    ;; insert agenda file
    (insert-file-contents cal-file)
    ;; turn on colours
    (display-ansi-colors)
    ;; turn on special mode
    (special-mode)
    ;; turn off line wrapping
    (visual-line-mode -1)))

;; -------------------- mu4e
(require 'org-mime)

(add-to-list 'load-path "/usr/local/Cellar/mu/1.2.0_1/share/emacs/site-lisp/mu/mu4e")
(require 'mu4e)

(setq mu4e-maildir (expand-file-name "~/.mail"))

; get mail
(setq mu4e-get-mail-command "mbsync -c ~/.emacs.d/mu4e/.mbsyncrc -a"
  ;; mu4e-html2text-command "w3m -T text/html" ;;using the default mu4e-shr2text
  mu4e-view-prefer-html t
  mu4e-update-interval 180
  mu4e-headers-auto-update t
  mu4e-compose-signature-auto-include nil
  mu4e-compose-format-flowed t)

;; to view selected message in the browser, no signin, just html mail
(add-to-list 'mu4e-view-actions
   '("ViewInBrowser" . mu4e-action-view-in-browser) t)

;; enable inline images
(setq mu4e-view-show-images t)
;; use imagemagick, if available
(when (fboundp 'imagemagick-register-types)
  (imagemagick-register-types))

;; every new email composition gets its own frame!
(setq mu4e-compose-in-new-frame t)

;; don't save message to Sent Messages, IMAP takes care of this
(setq mu4e-sent-messages-behavior 'delete)

(add-hook 'mu4e-view-mode-hook #'visual-line-mode)

;; <tab> to navigate to links, <RET> to open them in browser
(add-hook 'mu4e-view-mode-hook
  (lambda()
;; try to emulate some of the eww key-bindings
(local-set-key (kbd "<RET>") 'mu4e~view-browse-url-from-binding)
(local-set-key (kbd "<tab>") 'shr-next-link)
(local-set-key (kbd "<backtab>") 'shr-previous-link)))

;; from https://www.reddit.com/r/emacs/comments/bfsck6/mu4e_for_dummies/elgoumx
(add-hook 'mu4e-headers-mode-hook
      (defun my/mu4e-change-headers ()
        (interactive)
        (setq mu4e-headers-fields
          `((:human-date . 25) ;; alternatively, use :date
          (:flags . 6)
          (:from . 22)
          (:thread-subject . ,(- (window-body-width) 70)) ;; alternatively, use :subject
          (:size . 7)))))

;; if you use date instead of human-date in the above, use this setting
;; give me ISO(ish) format date-time stamps in the header list
;(setq mu4e-headers-date-format "%Y-%m-%d %H:%M")

;; spell check
(add-hook 'mu4e-compose-mode-hook
    (defun my-do-compose-stuff ()
       "My settings for message composition."
       (visual-line-mode)
       (org-mu4e-compose-org-mode)
           (use-hard-newlines -1)
       (flyspell-mode)))

(require 'smtpmail)

;;rename files when moving
;;NEEDED FOR MBSYNC
(setq mu4e-change-filenames-when-moving t)

;;set up queue for offline email
;;use mu mkdir  ~/Maildir/acc/queue to set up first
(setq smtpmail-queue-mail nil)  ;; start in normal mode

;;from the info manual
(setq mu4e-attachment-dir  "~/Downloads")

(setq message-kill-buffer-on-exit t)
(setq mu4e-compose-dont-reply-to-self t)

(require 'org-mu4e)

;; convert org mode to HTML automatically
(setq org-mu4e-convert-to-html t)

;;from vxlabs config
;; show full addresses in view message (instead of just names)
;; toggle per name with M-RET
(setq mu4e-view-show-addresses 't)

;; don't ask when quitting
(setq mu4e-confirm-quit nil)

;; mu4e-context
(setq mu4e-context-policy 'pick-first)
(setq mu4e-compose-context-policy 'always-ask)
(setq mu4e-contexts
  (list
    (make-mu4e-context
      :name "work" ;;for acc1-gmail
      :enter-func (lambda () (mu4e-message "Entering context work"))
      :leave-func (lambda () (mu4e-message "Leaving context work"))
      :match-func (lambda (msg)
        (when msg
          (mu4e-message-contact-field-matches
             msg '(:from :to :cc :bcc) "acc1@gmail.com")))
      :vars '((user-mail-address . "acc1@gmail.com")
        (user-full-name . "User Account1")
        (mu4e-sent-folder . "/acc1-gmail/[acc1].Sent Mail")
        (mu4e-drafts-folder . "/acc1-gmail/[acc1].drafts")
        (mu4e-trash-folder . "/acc1-gmail/[acc1].Bin")
        (mu4e-compose-signature . (concat "Formal Signature\n" "Emacs 25, org-mode 9, mu4e 1.0\n"))
        (mu4e-compose-format-flowed . t)
        (smtpmail-queue-dir . "~/.mail/acc1-gmail/queue/cur")
        (message-send-mail-function . smtpmail-send-it)
        (smtpmail-smtp-user . "acc1")
        (smtpmail-starttls-credentials . (("smtp.gmail.com" 587 nil nil)))
        (smtpmail-auth-credentials . (expand-file-name "~/.authinfo.gpg"))
        (smtpmail-default-smtp-server . "smtp.gmail.com")
        (smtpmail-smtp-server . "smtp.gmail.com")
        (smtpmail-smtp-service . 587)
        (smtpmail-debug-info . t)
        (smtpmail-debug-verbose . t)
        (mu4e-maildir-shortcuts . ( ("/acc1-gmail/INBOX"            . ?i)
                                    ("/acc1-gmail/[acc1].Sent Mail" . ?s)
                                    ("/acc1-gmail/[acc1].Bin"       . ?t)
                                    ("/acc1-gmail/[acc1].All Mail"  . ?a)
                                    ("/acc1-gmail/[acc1].Starred"   . ?r)
                                    ("/acc1-gmail/[acc1].drafts"    . ?d)))))))

;; -------------------- custom
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (csv-mode org-mime zop-to-char zenburn-theme which-key web-mode volatile-highlights super-save smex smartrep smartparens rainbow-mode rainbow-delimiters org-gcal operate-on-number mu4e-overview mu4e-alert move-text key-chord json-mode js2-mode imenu-anywhere ido-completing-read+ htmlize hl-todo helm-projectile helm-descbinds helm-ag guru-mode gotest go-projectile gitignore-mode gitconfig-mode git-timemachine gist geiser flycheck flx-ido expand-region exec-path-from-shell evil-visualstar evil-surround evil-python-movement evil-org evil-numbers evil-mu4e evil-magit evil-leader elisp-slime-nav editorconfig easy-kill discover-my-major diminish diff-hl deft crux company-go company-anaconda browse-kill-ring beacon anzu ace-window))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
