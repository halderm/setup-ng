default: help
.PHONY: mac_bootstrap debian_bootstrap desktop
OS := $(shell uname)

help:
	@echo "make mac_bootstrap - bootstrap on mac"
	@echo "make debian_bootstrap - bootstrap on debian"
	@echo "make server - call server playbook"
	@echo "make desktop - call desktop playbook"

mac_bootstrap:
	brew install ansible
	brew install python3
	brew install curl
	pip3 install --user ansible-lint
	pip3 install --user passlib
	PYCURL_SSL_LIBRARY=openssl pip3 install --user pycurl

debian_bootstrap:
	sudo apt install -y git ansible python3-dev python3-pip curl libcurl4-openssl-dev libssl-dev
	pip3 install --user wheel
	pip3 install --user ansible-lint
	pip3 install --user passlib
	PYCURL_SSL_LIBRARY=openssl pip3 install --user pycurl

server:
ifeq ($(OS),Linux)
	cd ansible && ansible-playbook -K -i inventory server.yml
else
	cd ansible && ansible-playbook -i inventory server.yml
endif

desktop:
ifeq ($(OS),Linux)
	cd ansible && ansible-playbook -K -i inventory desktop.yml
endif
