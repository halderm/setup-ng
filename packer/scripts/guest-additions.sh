#!/bin/bash

# mount iso
mount -o loop /home/vagrant/VBoxGuestAdditions.iso /mnt

# run installer
/mnt/VBoxLinuxAdditions.run

# umount and delete iso
umount /mnt
rm /home/vagrant/VBoxGuestAdditions.iso
