#!/bin/bash

# create .ssh directory
mkdir -p /home/vagrant/.ssh
chmod 0700 /home/vagrant/.ssh

# get vagrant inscecure key
wget --no-check-certificate \
  https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub \
  -O /home/vagrant/.ssh/authorized_keys

# fix user and permission of key
chmod 0600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant /home/vagrant/.ssh
