#!/bin/bash

# apt cleanup
apt autoremove
apt update

# delete unneeded files
rm -f /home/vagrant/*.sh

# add sync so packer doesn't quit too early
sync
