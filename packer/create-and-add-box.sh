#!/bin/bash

# create vagrant box
packer build ubuntu-16.04-vagrant.json
packer build ubuntu-18.04-vagrant.json

# add vagrant box
vagrant box add --force ubuntu/server16 output/ubuntu-16.04.6.box
vagrant box add --force ubuntu/server18 output/ubuntu-18.04.2.box
