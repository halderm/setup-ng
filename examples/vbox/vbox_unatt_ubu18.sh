#!/bin/bash
VBoxManage createvm --name ubuntu18server --ostype Ubuntu_64 --register
VBoxManage createmedium --filename ${HOME}/VirtualBox\ VMs/ubuntu18server/ubuntu18server.vdi --size 5000
VBoxManage storagectl ubuntu18server --name SATA --add SATA --controller IntelAhci
VBoxManage storageattach ubuntu18server --storagectl SATA --port 0 --device 0 --type hdd --medium ${HOME}/VirtualBox\ VMs/ubuntu18server/ubuntu18server.vdi
VBoxManage storagectl ubuntu18server --name IDE --add ide
VBoxManage storageattach ubuntu18server --storagectl IDE --port 0 --device 0 --type dvddrive --medium ${HOME}/Downloads/ubuntu-18.04.2-server-amd64.iso
VBoxManage modifyvm ubuntu18server --memory 1024 --vram 16
VBoxManage modifyvm ubuntu18server --ioapic on
VBoxManage modifyvm ubuntu18server --boot1 dvd --boot2 disk --boot3 none --boot4 none
VBoxManage modifyvm ubuntu18server --cpus 2
VBoxManage modifyvm ubuntu18server --audio none
VBoxManage modifyvm ubuntu18server --usb off
VBoxManage modifyvm ubuntu18server --usbehci off
VBoxManage modifyvm ubuntu18server --usbxhci off
VBoxManage modifyvm ubuntu18server --nic1 bridged --bridgeadapter1 en0 --nic2 nat
VBoxManage unattended install ubuntu18server --iso=${HOME}/Downloads/ubuntu-18.04.2-server-amd64.iso --user=halderm --password=pass --locale=en_US --country=CH --time-zone=UTC+1 --hostname=ubu18server.local.lan --language=en --start-vm=gui
