# set debian changelog variables
export DEBFULLNAME='Martin Halder'
export DEBEMAIL='martin.halder@emenda.ch'

# set default editor
export EDITOR='nvim'
export VISUAL='nvim'

# switch of annoying beep
setopt nobeep

# take extension and completion to its limit
setopt extendedglob

# do not use first match automatically
unsetopt menucomplete

# list choices of completion
setopt autolist

# no auto correction
unsetopt correct_all

# exit shell with background jobs
setopt nocheckjobs
setopt nohup

# use vv instead of esc for vim mode
bindkey -v
bindkey -M viins 'vv' vi-cmd-mode

# enable autojump
[ -f /usr/local/etc/profile.d/autojump.sh ] && . /usr/local/etc/profile.d/autojump.sh
