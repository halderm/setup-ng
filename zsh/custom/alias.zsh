if [ $(uname) = 'Darwin' ]; then

    # convenient ls
    alias l='ls -G -l -h'
    alias ls='ls -G'
    alias la='ls -G -l -A'
    alias l.='ls -l -d .[^.]*'

else

    # convenient ls
    alias l='ls --color -l -h'
    alias ls='ls --color'
    alias la='ls --color -l -A'
    alias l.='ls -l -d .*'

    # ubuntu convenience
    alias ka='killall -9 chrome &>/dev/null; pulseaudio -k &>/dev/null'
    alias pd='poweroff'

    # rescale backgroun image
    alias back='bash -c "feh --bg-scale ~/setup/images/background.png"'

fi

# directory ops
alias md='mkdir -p'
alias s='cd ..'
alias ss='cd ../..'
alias e='exit'

# vim
alias v='nvim '
alias vi='nvim '
alias vim='nvim '
alias vs='nvim .git/index'

# clear
alias c='clear'
alias cl='clear; l'
alias cls='clear; ls'

# git
alias ungit="find . -name '.git' -exec rm -rf {} \;"
alias gb='git --no-pager branch'
alias gba='git --no-pager branch -a'
alias gbd='git branch -d'
alias gc='git commit -v'
alias gca='git commit -v -a'
alias gls='git ls-files'
alias gcx='git clean -xdf'
alias gac='git update-index --assume-unchanged'
alias ganc='git update-index --no-assume-unchanged'
alias sac='git ls-files -v | grep "^[[:lower:]]"'
alias gh='bash -c "source ~/.githelpers && show_git_head"'
alias gs='bash -c "source ~/.githelpers && pretty_git_log"'
alias gsb='bash -c "source ~/.githelpers && pretty_git_branch_sorted"'
alias gco='git checkout'
alias gd='git diff'
alias gdm='git diff master'
alias gl='git pull'
alias glo='git pull origin master'
alias gp='git push'
alias gpo='git push origin master'
alias g='git status'
alias gk='git --no-pager log --pretty=oneline --all --decorate -30 --graph --abbrev-commit'
alias gkb='git --no-pager log --pretty=oneline --decorate -30 --graph --abbrev-commit'
alias gkn='git log --pretty=oneline --all --decorate --graph --abbrev-commit'
alias glg='git lg'

# finding grep
find_function() {
    find . -name "*$1*"
}
alias afind='ack -il'
alias ffind='find_function'

# lxc containers
alias lc='sudo lxc-ls -f'
alias lcs='sudo lxc-start -d -n '
alias lcp='sudo lxc-stop -n '
alias lcd='sudo lxc-destroy -f -n '
alias lcd='sudo lxc-destroy -f -n '

# docker containers
alias drc='docker rm -f $(docker ps -a -q)'
alias dri='docker rmi -f $(docker images -q)'

# kubectl
alias k='kubectl'
alias kx='kubectx'
